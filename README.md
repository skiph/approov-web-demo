# Approov Interactive Web Demo

![Approov Web Demo](./notes/approov-web-demo.png)

The Approov Web Demo provides an interactive animation of some of the main features of Approov.

## Installation

The Approov Web Demo is a react web app which can run locally in Windows, Mac, and Linux desktop environments.

To install the app:

1. Install a `node` javascript runtime and its package manager, `npm`.

Follow these [installation instructions](https://kinsta.com/blog/how-to-install-node-js/) to install and verify `node` and `npm` for your operating environment.

2. Install the `git` distributed version control system.

Follow these [installation instructions](https://github.com/git-guides/install-git) to install and verify `git` for your operating environment.

3. Clone the Approov Web Demo Repository

Open a command line terminal and change to a directory which will hold a clone of the Approov Web Demo repository.

Execute the git clone command:

```
git clone https://gitlab.com/skiph/approov-web-demo.git
```

4. Install the Approov Web Demo

In the command line terminal, change into the Approov Web Demo repository, and use the node package manager to complete the installation:

```
cd approov-web-demo

npm install
```

Note: the command to change directories is `dir` on windows and `cd` on macos and linux.

There may be many warnings issued during installation, but they should not break the installation.

## Usage

To run the demo, in the command line terminal, change into the Approov Web Demo repository and use the node package manager to run the application:

```
cd approov-web-demo

npm start
```

This should automatically open your browser. The first time, you may need to accept some permissions and/or select the browser to use.

The browser tab should be pointing to the running app. In the browser tab, you should see URL:

```
http://localhost:3000
```

If the app is running but no browser tab opens, try manually opening the location `localhost:3000` in your browser.

There are three pull down buttons at the bottom of the application. Each pull down has some actions you can switch through to describe how Approov works. A minimal sequence might be:

- Reset and Launch App
- Steal API Key
- Launch BotNet
- Integrate Approov
- Register App with Approov
- Enable Approov Checking

Play around with the actions in different sequences to see how the display changes.

Send any questions to [skip.hovsmith@approov.io](mailto:skip.hovsmith@approov.io)

---
