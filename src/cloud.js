import React from 'react'
import { Center, Image } from '@chakra-ui/react'
import { useStage } from './stage-context'
import cloud from './assets/cloud.png'
import cloudPass from './assets/cloud-pass.png'
import cloudFail from './assets/cloud-fail.png'

const Cloud = () => {
  const [stage, dispatch] = useStage()
  const {connecting, isPinning} = stage.cloud
  const isIntegrated = stage.client.isIntegrated
  const isApprooving = stage.approov.isApprooving

  const debug = `connecting: ${connecting}, isPinning: ${isPinning}`
  console.log(`cloud: ${debug}`)

  if (connecting.length) {
    if (isIntegrated) {
      if (isApprooving) {
        return (
          <Center w="40vw"  h="20vw">
            <Image w="full" h="full" objectFit="contain" src={cloudPass} alt="Shapes Service" />      
          </Center>
        )
      } else {
        return (
          <Center w="40vw"  h="20vw">
            <Image w="full" h="full" objectFit="contain" src={cloudFail} alt="Shapes Service" />      
          </Center>
        )
      }
    } else {
      return (
        <Center w="40vw"  h="20vw">
          <Image w="full" h="full" objectFit="contain" src={cloud} alt="Shapes Service" />      
        </Center>
      )
    }
  } else {
    return (
      <Center w="40vw"  h="20vw">
      </Center>
    )
  }
}

export default Cloud
