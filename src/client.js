import React, { useState, useEffect } from 'react'
import { Box, Button, Center, Flex, Heading, Image, Text } from '@chakra-ui/react'
import { useStage } from './stage-context'
import logo from './assets/approov.png'

const appTitle = 'Approov Shapes'
const checkTitle = 'check'
const fetchTitle = 'fetch'

const Client = () => {
  const [stage, dispatch, fetchShape] = useStage()
  const {isRunning, isLaunching, isIntegrated} = stage.client

  const initialShape = {
    name: 'logo',
    img: logo,
    msg: 'Welcome!',
  }

  const [shape, setShape] = useState(initialShape)
  const [tick, setTick] = useState(0)

  useEffect(() => {
    const loaded = true
    const nextTick = async () => {
      // if (isLaunching) {
      //   setTick(0)
      //   setShape(initialShape)
      //   dispatch({actor:'client', cmd:'launched'})
      // }
      try {
        const nextShape = fetchShape(true)
        setTimeout(() => {
          loaded && setTick(tick + 1)
          loaded && setShape(nextShape)
        }, 1250)
      } catch (e) {
        console.log(e)
      }
    }
    nextTick()
  }, [tick])

  const integration = !stage.client.isIntegrated? 'Approov not installed' : 'Approov integrated'
  const debug = `isRunning: ${isRunning}, isIntegrated: ${isIntegrated}`

  if (isRunning) {
    if (isIntegrated) {
      return (
        <Box w="30vw" h="50vw" py="5vw" pr="5vw">
          <Flex h="full"
              borderColor="gray.700" borderRadius="3xl" 
              borderTopWidth="3vw" borderBottomWidth="3vw" borderLeftWidth="1px" borderRightWidth="1px" 
              direction="column" justify="space-between">
            <Text pt="1vw" fontSize="3xl" textAlign="center">{appTitle}</Text>
            <Flex pt="2vw" px="5vw" justify="center">
              <Image objectFit="contain" src={shape.img} alt={shape.name} />
            </Flex>
            <Text pt="1vw" fontSize="xl" color="black.700" textAlign="center" >{shape.msg}</Text>
            <Box pt="1vw">
              <Flex justify="space-around">
                <Button colorScheme="blue">{checkTitle}</Button>
                <Button colorScheme="blue">{fetchTitle}</Button>
              </Flex>
            </Box>
            <Text pb=".5vw" color="red.700" textAlign="center" >{integration}</Text>
          </Flex>
        </Box>
      )
    } else {
      return (
        <Box w="30vw" h="50vw" py="5vw" pr="5vw">
          <Flex h="full"
              borderColor="gray.700" borderRadius="3xl" 
              borderTopWidth="3vw" borderBottomWidth="3vw" borderLeftWidth="1px" borderRightWidth="1px" 
              direction="column" justify="space-between">
            <Text pt="1vw" fontSize="3xl" textAlign="center">{appTitle}</Text>
            <Flex pt="2vw" px="5vw" justify="center">
              <Image objectFit="contain" src={shape.img} alt={shape.name} />
            </Flex>
            <Text pt="1vw" fontSize="xl" color="black.700" textAlign="center" >{shape.msg}</Text>
            <Box pt="1vw">
              <Flex justify="space-around">
                <Button colorScheme="blue">{checkTitle}</Button>
                <Button colorScheme="blue">{fetchTitle}</Button>
              </Flex>
            </Box>
            <Text pb=".5vw" color="red.700" textAlign="center" >{integration}</Text>
          </Flex>
        </Box>
      )
    }
  } else {
    return (
      <Box w="30vw" h="50vw" py="5vw" pr="5vw">
        <Flex h="full"
            borderColor="gray.700" borderRadius="3xl" 
            borderTopWidth="3vw" borderBottomWidth="3vw" borderLeftWidth="1px" borderRightWidth="1px" 
            direction="column" justify="space-between">
        </Flex>
      </Box>
    )

  }
}

export default Client
