import React from 'react'
import { HStack, VStack } from '@chakra-ui/react'
import Client from './client'
import Server from './server'
import Approov from './approov'
import Bot from './bot'
import Cloud from './cloud'

const Display = () => {
  return (
    <HStack w="100vw" h="50vw" spacing="0">
      <Bot/>

      <VStack spacing="0">
        <Approov />

        <Cloud />

        <Server />
      </VStack>

      <Client />
    </HStack>
  )
}

export default Display
