import React, { useState, useEffect } from 'react'
import { Box, Center, Flex, Grid, GridItem, Image, Text, VStack } from '@chakra-ui/react'
import { useStage } from './stage-context'
import MotionBox from './motionbox'
import botNetLogo from './assets/botnet.png'
import logo from './assets/approov.png'
import stats from './assets/static.png'
import mitm from './assets/mitm.png'

const botSlotDelay = 1.2
const botSlotDuration = 1

const BotSlot = ({i, r, c, d, h, s, ...rest}) => {
  return (
    <GridItem rowStart={r} colStart={c} colSpan={4}>
      <MotionBox position="relative" w="full" h={h}bg="black"
          animate={{opacity:1}} initial={{opacity:0}} transition={{delay:d*botSlotDelay, duration:botSlotDuration}} >
        <VStack w="full" h="full">
          <Image w="50%" h="50%" pt="1vw" fit="contain" src={s.img} />
          <Text fontSize="sm" color="white">{s.msg}</Text>
        </VStack>
      </MotionBox>
    </GridItem>
  )
}

const Bot = () => {
  const gridWidth = 25
  const boxHeight = 5
  const gapHeight = 2
  const gridH = `${5*boxHeight + 4*gapHeight}vw`
  const gridW = `${gridWidth}vw`
  const gap = `${gapHeight}vw`
  const boxH = `${boxHeight}vw`

  const initialShape = {
    name: 'logo',
    img: logo,
    msg: 'Welcome!',
  }
  const initialShapes = [
    initialShape, initialShape, initialShape, initialShape, 
    initialShape, initialShape, initialShape, initialShape
  ]

  const [stage, dispatch, fetchShape] = useStage()
  const {isStealing, isRunning} = stage.bot

  const [shapes, setShapes] = useState(initialShapes)
  const [tick, setTick] = useState(0)

  useEffect(() => {
    const loaded = true
    const nextTick = async () => {
      try {
        const nextShape = fetchShape(false)
        console.log(`tick: ${tick%8}, shape: ${nextShape.name}`)
        let nextShapes = shapes
        nextShapes[tick%8] = nextShape
        setTimeout(() => {
          loaded && setTick(tick + 1)
          loaded && setShapes(nextShapes)
        }, 15)
      } catch (e) {
        console.log(e)
      }
    }
    if (isRunning) {
      nextTick()
    } else {
      setTick(0)
    }
  }, [tick, isRunning])

  const debug = `isStealing: ${isStealing}, isRunning: ${isRunning}`

  if (isRunning) {
    return (
      <Center w="30vw"  h="50vw" pl="5vw">
        <Box position="relative" bg="white">
          <Grid w={gridW} h={gridH} templateRows="repeat(5, 1fr)" templateColumns="repeat(12, 1fr)" 
            gap={gap}>
            <BotSlot i={2} r={1} c={5} d={2} h={boxH} s={shapes[2]} />
            <BotSlot i={4} r={2} c={2} d={3} h={boxH} s={shapes[4]} />
            <BotSlot i={6} r={2} c={8} d={3} h={boxH} s={shapes[6]} />
            <BotSlot i={0} r={3} c={1} d={1} h={boxH} s={shapes[0]} />
            <BotSlot i={1} r={3} c={9} d={0} h={boxH} s={shapes[1]} />
            <BotSlot i={7} r={4} c={2} d={3} h={boxH} s={shapes[7]} />
            <BotSlot i={5} r={4} c={8} d={3} h={boxH} s={shapes[5]} />
            <BotSlot i={3} r={5} c={5} d={2} h={boxH} s={shapes[3]} />
          </Grid>
          <MotionBox position="absolute" top={0} left={0} w={gridW} h={gridH}
              animate={{opacity:1}} initial={{opacity:0}} transition={{delay:3*botSlotDelay, duration:botSlotDuration}}>
            <Center w="full" h="full">
            <Image w="25%" h="25%" fit="contain" src={botNetLogo} />
            </Center>
          </MotionBox>
        </Box>
      </Center>
    )
  } else if (isStealing) {  
    return (
      <Center w="30vw"  h="25vw" pl="5vw">
          <Flex h="full"
              direction="column" justify="space-between">
            <Box>
              <Text pt="1vw" fontSize="3xl" textAlign="center">Static Analysis</Text>
              <Flex pt="2vw" px="5vw" justify="center">
                <Image objectFit="contain" src={stats} alt="Static Analysis" />
              </Flex>
            </Box>
            <Box>
              <Text pt="1vw" fontSize="3xl" textAlign="center">In-the-Middle Attack</Text>
              <Flex pt="2vw" px="5vw" justify="center">
                <Image objectFit="contain" src={mitm} alt="MitM Attack" />
              </Flex>
            </Box>
          </Flex>
      </Center>
    )
  } else {
    return (
      <Center w="30vw"  h="50vw" pl="5vw">
      </Center>
    )

  }
}

export default Bot
