import React from 'react'
import { ChakraProvider, theme, Box } from '@chakra-ui/react'
import { StageProvider } from './stage-context'
import Display from './display'
import Controls from './controls'

const App = () => {
  return (
    <ChakraProvider theme={theme}>
      <StageProvider>
        <Box>
          <Display />
          <Controls />
        </Box>
      </StageProvider>
    </ChakraProvider>
  )
}

export default App
