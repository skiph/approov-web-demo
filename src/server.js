import React from 'react'
import { Center, Image } from '@chakra-ui/react'
import { useStage } from './stage-context'
import shapesService from './assets/server.png'
import shapesServiceChecking from './assets/server-check.png'

const Server = () => {
  const [stage, dispatch] = useStage()
  const {isRunning, isChecking} = stage.server
 
  const debug = `isRunning: ${isRunning}, isChecking: ${isChecking}`

  if (isRunning) {
    if (!isChecking) {
      return (
        <Center w="30vw"  h="15vw">
          <Image pb="5vw" w="full" h="full" objectFit="contain" src={shapesService} alt="Shapes Service" />
        </Center>
      )
    } else {
      return (
        <Center w="30vw"  h="15vw">
          <Image pb="5vw" w="full" h="full" objectFit="contain" src={shapesServiceChecking} alt="Shapes Service" />
        </Center>
      )
    }
  } else {
    return (
      <Center w="30vw">
      </Center>
    )
  }
}

export default Server
