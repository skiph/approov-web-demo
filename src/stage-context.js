import React, { useReducer, useContext, useMemo } from 'react'
import logo from './assets/approov.png'
import hello from './assets/hello.png'
import confused from './assets/confused.png'
import rectangle from './assets/rectangle.png'
import square from './assets/square.png'
import triangle from './assets/triangle.png'
import circle from './assets/circle.png'

const imgAssets = {logo, hello, confused, rectangle, square, triangle, circle}

const initialStage = {
  client: {
    isRunning: false,
    isIntegrated: false,
  },
  server: {
    isRunning: false,
    isChecking: false,
  },
  approov: {
    isRunning: false,
    isLaunching: false,
    isApprooving: false,
  },
  bot: {
    isRunning: false,
    isStealing: false,
  },
  cloud: {
    connecting: '',
    isPinning: false,
  },
}

const cloudConnections = ['', 'client-server', 'client-server-bot', 'client-server-approov', 'client-server-approov-bot']

const StageReducer = (state, action) => {
  switch (action.actor) {
    case 'client':
      switch (action.cmd) {
        case 'launch':
          return {
            client: { isRunning: true, isLaunching: true, isIntegrated: false },
            server: { isRunning: true, isChecking: false },
            approov: { isRunning: false, isApprooving: false },
            bot: { isRunning: false, isStealing: false },
            cloud: { connecting: 'client-server', isPinning: false },
          }
        case 'integrate':
          return {
            ...state,
            client: { isRunning: true, isLaunching: true, isIntegrated: true },
            server: { ...state.server, isRunning: true },
            approov: { isRunning: true, isApprooving: false },
            cloud: { connecting: state.bot.isRunning? 'client-server-approov-bot' : 'client-server-approov', isPinning:true },
          }
        case 'register':
          if (state.client.isIntegrated) {
            return {
              ...state,
              approov: { isRunning: true, sisLaunching: false, isApprooving: true },
            }
          } else {
            throw new Error(`Cannot register client before integrating Approov`)
          }        
        default: 
          throw new Error(`Unrecognized ${action.actor} command: ${action.cmd}`)
      }
    case 'server':
      switch (action.cmd) {
        case 'pass':
          return {
            ...state,
            server: { isRunning: true, isChecking: false },
          }
          case 'check':
            return {
              ...state,
              server: { isRunning: true, isChecking: true },
            }
        default: 
          throw new Error(`Unrecognized ${action.actor} command: ${action.cmd}`)
      }
    case 'bot':
      switch (action.cmd) {
        case 'steal':
          return {
            ...state,
            bot: { isRunning: false, isStealing: true },
          }
        case 'launch':
          return {
            ...state,
            client: { ...state.client, isRunning: true, isLaunching: !state.client.isRunning },
            server: { ...state.server, isRunning: true },
            bot: { isRunning: true, isStealing: false },
            cloud: { ...state.cloud, connecting: state.approov.isRunning? 'client-server-approov-bot' : 'client-server-bot' },
          }
        case 'quit':
          return {
            ...state,
            bot: { isRunning: false, isStealing: false },
            cloud: { ...state.cloud, connecting: state.approov.isRunning? 'client-server-approov' : 'client-server' },
          }
          default: 
          throw new Error(`Unrecognized ${action.actor} command: ${action.cmd}`)
      }
    default: 
      throw new Error(`Unrecognized actor ${action.actor}`)
  }
}

const StageContext = React.createContext()

const useStage = () => {
  const context = useContext(StageContext)
  if (!context) {
    throw new Error(`useStage() must be used within a StageProvider`)
  }
  return context
}

const StageProvider = props => {
  const [stage, dispatch] = useReducer(StageReducer, initialStage)
  const fetchShape = (token) => {
    let name = 'confused'
    let msg = '401: Unauthorized'
    if (!stage.server.isChecking || stage.approov.isApprooving && token) {
      const shapeNames = [ 'circle', 'rectangle', 'square', 'triangle' ]
      name = shapeNames[Math.floor((Math.random() * shapeNames.length))]
      msg = `200: OK`
    }
    return ({
      name,
      img: imgAssets[name],
      msg,
    })
  }

  // const value = useMemo(() => [stage, dispatch], [stage])
  const value = [stage, dispatch, fetchShape]

  return <StageContext.Provider value={value} {...props} />
}

export {StageProvider, useStage}
