import React from 'react'
import { Box, Button, Center, Flex, Menu, MenuButton, MenuItem, MenuList } from '@chakra-ui/react'
import { useStage } from './stage-context'

const Controls = () => {
  const [stage, dispatch] = useStage()

  return (
    <Flex w="100vw"  h="6.25vw">
      <Center w="30vw" pl="5vw">
      <Menu>
          <MenuButton as={Button}>
            Fraudster
          </MenuButton>
          <MenuList>
            <MenuItem onClick={() => dispatch({actor:'bot', cmd:'steal'})} isDisabled={stage.bot.isStealing}>Steal API Key</MenuItem>
            <MenuItem onClick={() => dispatch({actor:'bot', cmd:'launch'})} isDisabled={stage.bot.isRunning}>Launch BotNet</MenuItem>
            <MenuItem onClick={() => dispatch({actor:'bot', cmd:'quit'})} isDisabled={!stage.server.isChecking}>Move On</MenuItem>
          </MenuList>
        </Menu>
      </Center>
      <Center w="40vw">
      <Menu>
          <MenuButton as={Button}>
            Shapes Service
          </MenuButton>
          <MenuList>
            <MenuItem onClick={() => dispatch({actor:'server', cmd:'pass'})} isDisabled={!stage.server.isChecking}>Disable Approov Checking</MenuItem>
            <MenuItem onClick={() => dispatch({actor:'server', cmd:'check'})} isDisabled={stage.server.isChecking}>Enable Approov Checking</MenuItem>
          </MenuList>
        </Menu>
      </Center>
      <Center w="30vw" pr="5vw">
        <Menu>
          <MenuButton as={Button}>
            Shapes App
          </MenuButton>
          <MenuList>
            <MenuItem onClick={() => dispatch({actor:'client', cmd:'launch'})}>Reset and Launch App</MenuItem>
            <MenuItem onClick={() => dispatch({actor:'client', cmd:'integrate'})}>Integrate Approov</MenuItem>
            <MenuItem onClick={() => dispatch({actor:'client', cmd:'register'})} isDisabled={!stage.client.isIntegrated}>Register App with Approov</MenuItem>
          </MenuList>
        </Menu>
      </Center>
    </Flex>
  )
}

export default Controls
