import React from 'react'
import { Center, Image } from '@chakra-ui/react'
import { useStage } from './stage-context'
import approovService from './assets/approov-running.png'

const Approov = () => {
  const [stage, dispatch] = useStage()
  const {isRunning, isApprooving} = stage.approov

  const debug = `isRunning: ${isRunning}, isApprooving: ${isApprooving}`

  if (isRunning) {
    return (
      <Center w="30vw"  h="15vw">
        <Image pt="5vw" w="full" h="full" objectFit="contain" src={approovService} alt="Approov Service" />
      </Center>
    )
  } else {
    return (
      <Center w="30vw"  h="15vw">
      </Center>
    )
  }
}

export default Approov
